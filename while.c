//
// Author By wangcw .
// Generate At 2023/11/8 09:42
//

#include <stdio.h>

int main ()
{
    /* 局部变量定义 */
    int a = 1;

    /* while 循环执行 */
    while( a < 10 )
    {
        printf("a 的值： %d\n", a);
        a++;
    }

    return 0;
}
