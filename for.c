//
// Author By wangcw .
// Generate At 2023/11/8 09:40
//

#include <stdio.h>

int main ()
{
    /* for 循环执行 */
    for( int a = 1; a < 10; a = a + 1 )
    {
        printf("a 的值： %d\n", a);
    }

    return 0;
}
