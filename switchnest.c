//
// Author By wangcw .
// Generate At 2023/11/8 09:55
//

#include <stdio.h>

int main ()
{
    /* 局部变量定义 */
    int a = 100;
    int b = 200;

    switch(a) {
        case 100:
            printf("这是外部 switch 的一部分\n", a );
            switch(b) {
                case 200:
                    printf("这是内部 switch 的一部分\n", a );
            }
    }
    printf("a 的准确值是 %d\n", a );
    printf("b 的准确值是 %d\n", b );

    return 0;
}
