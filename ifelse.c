//
// Author By wangcw .
// Generate At 2023/11/8 09:52
//

#include <stdio.h>

int main ()
{
    /* 局部变量定义 */
    int a = 100;

    /* 检查布尔条件 */
    if( a < 20 )
    {
        /* 如果条件为真，则输出下面的语句 */
        printf("a 小于 20\n" );
    }
    else
    {
        /* 如果条件为假，则输出下面的语句 */
        printf("a 大于 20\n" );
    }
    printf("a 的值是 %d\n", a);

    return 0;
}
