//
// Author By wangcw .
// Generate At 2023/11/8 09:53
//

#include <stdio.h>

int main ()
{
    /* 局部变量定义 */
    char grade = 'B';

    switch(grade)
    {
        case 'A' :
            printf("很棒！\n" );
            break;
        case 'B' :
            // printf("%c\n", grade);
            // break;
        case 'C' :
            // printf("CCC%c\n", grade);
            printf("做得好\n" );
            break;
            // 当被测试的变量等于 case 中的常量时，case 后跟的语句将被执行，直到遇到 break 语句为止。
            // 所以才执行了这句话！！！
        case 'D' :
            printf("您通过了\n" );
            break;
        case 'F' :
            printf("最好再试一下\n" );
            break;
        default :
            printf("无效的成绩\n" );
    }
    printf("您的成绩是 %c\n", grade );

    return 0;
}
