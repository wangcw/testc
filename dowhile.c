//
// Author By wangcw .
// Generate At 2023/11/8 09:42
//

#include <stdio.h>

int main ()
{
    /* 局部变量定义 */
    int a = 1;

    /* do 循环执行 */
    do
    {
        printf("a 的值： %d\n", a);
        a = a + 1;
    }while( a < 10 );

    return 0;
}
