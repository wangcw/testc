//
// Author By wangcw .
// Generate At 2023/11/8 09:19
//

#include <stdio.h>

int main()
{
    int a = 5;
    int b = 20;
    int c ;

    if ( a && b ) //真 与 真 结果为真
    {
        printf("Line 1 - 条件为真\n" );
    }
    if ( a || b ) // 真 或 真 结果为真
    {
        printf("Line 2 - 条件为真\n" );
    }
    /* 改变 a 和 b 的值 */
    a = 0;
    b = 10;
    if ( a && b )  // 假 与 真 结果为假
    {
        printf("Line 3 - 条件为真\n" );
    }
    else
    {
        printf("Line 3 - 条件不为真\n" );
    }
    if ( !(a && b) ) // 非（假 与 真） 结果为真
    {
        printf("Line 4 - 条件为真\n" );
    }
}
